class Image < ActiveRecord::Base
  belongs_to :user
  validates :name, :presence => true
  validates :picture, :presence => true
  attr_accessible :name, :picture
  mount_uploader :picture, PictureUploader
  #validates :picture_size

=begin
  private
  def picture_size
    if picture.size > 5.megabytes
      errors.add(:picture, "should be less than 5mb")    
    end  
  end
=end
end
