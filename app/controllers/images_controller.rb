class ImagesController < ApplicationController
  before_filter :set_image, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @images = Image.all
    respond_with(@images)
  end

  def show
    respond_with(@image)
  end

  def new
    @image = Image.new
    #respond_with(@image)
  end

  def edit
  end

  def create
    binding.pry
    @image = Image.new(params[:image])
    @image.user = current_user
    if @image.save
      redirect_to images_path(@user)
    else
      render :new
    end
  end

  def update
    @image.update_attributes(params[:image])
    respond_with(@image)
  end

  def destroy
    @image.destroy
    respond_with(@image)
  end

  private
    def set_image
      @image = Image.find(params[:id])
    end
end
